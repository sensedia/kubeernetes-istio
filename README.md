
### Kubernetes

1. Add <istio-injection=enabled> label to your Namespace
```bash
kubectl label namespace <YOUR-NAME-HERE> istio-injection=enabled
```

2. Set your Namespace as default
```bash
kubectl config set-context --current --namespace <YOUR-NAME-HERE>
```

3. Deploy Bets application
```bash
kubectl apply --recursive -f ./k8s
```

### Istio

1. Gateway (remember to edit the host YAML with your name)
```bash
kubectl apply -f ./istio/gateways
```

2. Virtual Service
```bash
kubectl apply -f ./istio/virtual_services
```

3. DR
```bash
kubectl apply -f ./istio/destination_rules
```

4. Traffic Control
```bash
kubectl apply -f ./istio/release
```

5. Fault
```bash
kubectl apply -f ./istio/fault_injection
```

6. Authn
```bash
kubectl apply -f ./istio/authn
```

7. Authz
```bash
kubectl apply -f ./istio/authz
```

```
├── k8s/                            -> app deployment
├── istio/                          -> istio configurations
│   ├── gateways/
│   ├── virtual_services/
│   ├── destination_rules/
│   └── release/
│   ├── fault_injection/
│   ├── authn/
│   └── authz/
└── scripts/
    ├── config-your-host.sh         -> replace all YAML templates with your default namespace
    ├── fwd-kiali.sh                -> forward port 20001 to Kiali
    ├── set-default-namespace.sh    -> set your default namespace
    └── curl/
        ├── bets.sh                 -> requests to bets service
        └── keycloak.sh             -> requests to authenticate on keycloak server
```

List all Sensedia resources
```bash
kubectl get -n bets apiauthentications.security.sensedia.com,logcollectors.monitoring.sensedia.com,logs.monitoring.sensedia.com,logtemplates.monitoring.sensedia.com,meshes.management.sensedia.com,policies.security.sensedia.com,releases.networking.sensedia.com,serviceanalytics.analytics.sensedia.com,shadowtraffics.networking.sensedia.com
```

List all Istio resources
```bash
kubectl get -n bets adapters.config.istio.io,attributemanifests.config.istio.io,authorizationpolicies.security.istio.io,clusterrbacconfigs.rbac.istio.io,destinationrules.networking.istio.io,envoyfilters.networking.istio.io,gateways.networking.istio.io,handlers.config.istio.io,httpapispecbindings.config.istio.io,httpapispecs.config.istio.io,instances.config.istio.io,meshpolicies.authentication.istio.io,policies.authentication.istio.io,quotaspecbindings.config.istio.io,quotaspecs.config.istio.io,rbacconfigs.rbac.istio.io,rules.config.istio.io,serviceentries.networking.istio.io,servicerolebindings.rbac.istio.io,serviceroles.rbac.istio.io,sidecars.networking.istio.io,templates.config.istio.io,virtualservices.networking.istio.io
```